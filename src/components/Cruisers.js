import React from 'react';
import ReactDOM from 'react-dom';

export default class Cruisers extends React.Component {

    constructor() {
        super();
        this.state = { items: [] };
    }

    componentDidMount() {
        let cruiseGetKeys = [];
        let cruiseFiltObjArray = [];

        // couldn't fetch data so I made it local
        fetch('some.json') //, {mode: 'no-cors'})
            .then(
            function (response) {
                if (response.status !== 200) {
                    console.log('There was an error, status Code: ' +
                        response.status);
                    return;
                }
                // Get data from res
                response.json().then(function (data) {
                    // Get keys for iterating through object (UUID?)
                    cruiseGetKeys = Object.keys(data.results);

                    for (let i in cruiseGetKeys) {

                        //     console.log(`
                        //     ${data.results[cruiseGetKeys[i]].participants[0].firstname}
                        //     ${data.results[cruiseGetKeys[i]].participants[0].lastname}
                        //     ${data.results[cruiseGetKeys[i]].participants[0].email}`);

                        // Create filtered array
                        cruiseFiltObjArray[i] = {
                            // trackingNumber: parseInt(i) + 1, // not necessary, but for dev purposes 
                            firstname: data.results[cruiseGetKeys[i]].participants[0].firstname,
                            lastname: data.results[cruiseGetKeys[i]].participants[0].lastname,
                            email: data.results[cruiseGetKeys[i]].participants[0].email
                        }
                    }

                    for (let i in cruiseFiltObjArray) console.log(cruiseFiltObjArray[i]);

                });
            }
            )
            .catch(function (err) {
                console.log('There was an error: ', err);
            });

    }

    render() {
        return (
            <cruisers>cruisers go here (for now take a peek at console for the list)</cruisers>
        );
    }
}