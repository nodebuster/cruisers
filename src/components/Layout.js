import React from 'react';

import Header from './Header';
import Cruisers from './Cruisers';
import Footer from './Footer';


export default class Layout extends React.Component {

    render() {
        return (
            <div>    
                <Header />
                
                <Cruisers />
                <Footer />
            </div>
        );
    }
}